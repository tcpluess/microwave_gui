__author__ = 'David'
import time
from threading import Thread


class Output:
    ## Constructor
    def __init__(self,gui, can):
        self.m_can = can
        self.m_gui = gui
        self.pumpMess = [25617, 1, 100]

    #Init Button
    def btninit_Click(self):
        self.m_can.Init()
        self.m_can.write([129, 03], 000)        #reset I/O
        self.m_can.write([129, 04], 000)        #reset I/O
        time.sleep(0.1)
        if self.m_gui.t.isAlive() == False:
            self.m_gui.t = Thread(target=self.m_gui.prog, args=())
            self.m_gui.t.start()
        if self.m_can.t.isAlive() == False:
            self.m_can.t = Thread(target=self.m_can.communication, args=())
            self.m_can.t.start()
        self.m_can.write([01, 03], 000)         #I/O Operating
        self.m_can.write([01, 04], 000)         #I/O Operating
        self.m_can.write([34, 01, 24, 05, 02, 04, 00, 00], 1540)            #Analoginput zykus
        self.m_can.write([34, 00, 83, 00, 00, 00, 00, 00], 1540)            #Analoginput format

    #Release Button
    def btnrelease_Click(self):
        self.release()
        self.m_gui.writeInfo("release")

    #reset to default
    def release(self):
        self.m_can.microMessage[0] = 00
        self.m_can.write([00, 00, 00, 00, 00, 00, 00, 00], 771)
        self.m_can.write([00, 00], 515)
        self.m_can.Release()

    #Button Reset
    def btnreset_Click(self):
        if self.m_can.microdata[0] & 16 or self.m_gui.sigWaterlimit.get() == "red":
            self.m_can.microMessage[0] = 01
            self.m_can.write(self.m_can.microMessage, '16')
            self.m_can.microMessage[0] = 00
            self.m_gui.setColorWaterlimit("green")
            self.m_gui.writeInfo("Reset")
        else:
            self.m_gui.writeInfo("Kein Fehler")

    #Button Filament
    def btnfilament_Click(self):
        if self.m_gui.sigWaterlimit.get() == "red":
            self.m_gui.writeInfo("Wassertemperaturlimite")
        elif self.m_can.microdata[0] == 01:
            self.m_can.microMessage[0] = 02     #on

        elif self.m_can.microdata[0] == 03:
            self.m_can.microMessage[0] = 00     #off
        else:
            self.m_gui.writeInfo("CAN initialisiert? / Mikrowelle ausgeschaltet?")

    #Button Interlock
    def btninterlock_Click(self):
        if self.m_can.microdata[0] == 03:
            self.m_can.microMessage[0] = 10     #on
        else:
            self.m_gui.writeInfo("Vorheizen eingeschaltet? / Mikrowelle ausgeschaltet?")

    #Button Microwave
    def btnmicro_Click(self):
        if self.m_can.microdata[0] == 3 and self.m_can.microMessage[0] == 10 and self.m_can.pumpdata[1] == 03 and self.m_gui.watertemp.get() < 26:
            self.m_can.microMessage[1] = (self.m_gui.val2*220/100)      #set Powervalue
            self.m_can.write(self.m_can.microMessage, '16')
            self.m_can.microMessage[0] = 14                             #on
            self.m_gui.writeInfo("Mikrowelle schaltet ein")

        elif self.m_can.microdata[0] == 07:
            self.m_can.microMessage[0] = 3                              #off
            self.m_can.microMessage[3] = 0
            self.m_can.microMessage[4] = 0
        else:
            self.m_gui.writeInfo("Kuehlwasser und Interlock eingeschaltet?")

    #Button Pulsmode
    def btnpuls_Click(self):
        if self.m_can.microdata[0] == 03:
            self.m_can.microMessage[3] = (self.m_gui.val)               #Set Puls on Value
            self.m_can.microMessage[4] = (self.m_gui.val1)              #Set Puls off Value
            self.m_gui.writeInfo("Pulsmode")
        else:
            self.m_gui.writeInfo("Vorheizen eingeschaltet? / Mikrowelle ausgeschaltet?")

    #Button Pump
    def btnpump_Click(self):
        if self.m_can.pumpdata[1] == 00:
            self.m_can.write([00, 01], 515)                             #on
            self.m_can.write([00, 00, 232, 03, 00, 00, 00, 00], 771)    #max Value
        elif (self.m_can.pumpdata[1] == 03) and (self.m_can.microdata[0] < 4):
            self.m_can.write([00, 00, 00, 00, 00, 00, 00, 00], 771)     #Value = 0
            self.m_can.write([00, 00], 515)                             #off
        else :
            self.m_gui.writeInfo("Mikrowelle ausgeschaltet?")
