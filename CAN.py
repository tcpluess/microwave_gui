__author__ = 'David'

from PCANBasic import *        ## PCAN-Basic library import
from threading import Thread
import threading
from time import *
import time

class CAN:
    #Constuctor
    def __init__(self,gui):
        self.m_bus = PCAN_USBBUS1
        self.m_baudrate = PCAN_BAUD_500K
        self.m_hwtype = TPCANType(0)
        self.m_ioport = c_uint(0)
        self.m_interrupt = c_ushort(0)
        self.microdata = [00, 00, 00, 00, 00, 00, 00, 00]
        self.pumpdata = [00, 00, 00, 00, 00, 00, 00, 00]
        self.analogdata = [00, 00, 00, 00, 00, 00, 00, 00]
        self.nonamedata = [00, 00, 00, 00, 00, 00, 00, 00]
        self.microMessage = [00, 00, 00, 00, 00, 00, 00, 00]
        self.m_objPCANBasic = PCANBasic()
        self.m_gui = gui
        self.t = Thread(target=self.communication, args=())
        self.log = Thread(target=self.logFile, args=())
        self.lock = threading.Lock()
        self.lenghtLogFile = 9000

    #initialize CAN
    def Init(self):
        self.m_objPCANBasic.Initialize(self.m_bus, self.m_baudrate,self.m_hwtype,self.m_ioport,self.m_interrupt)

    #release CAN
    def Release(self):
        self.m_objPCANBasic.Uninitialize(self.m_bus)

    #Communication thread Read and Write 200ms
    def communication(self):
        while(self.GetStatus() < 10):
            result = self.m_objPCANBasic.Read(self.m_bus)
            if result[0] == PCAN_ERROR_OK:
                # We show the received message


                theMsg = result[1]
                for i in range(8):
                    if (144 == theMsg.ID):
                        with self.lock:
                            self.microdata[i] = theMsg.DATA[i]
                            if self.log.isAlive() == False and self.microdata[0] == 07:
                                self.log = Thread(target=self.logFile, args=())
                                self.log.start()
                    if (387 == theMsg.ID):
                        with self.lock:
                            self.pumpdata[i] = theMsg.DATA[i]
                    if (644 == theMsg.ID):
                        with self.lock:
                            self.analogdata[i] = theMsg.DATA[i]
                    else:
                        self.nonamedata[i] = theMsg.DATA[i]

                self.write(self.microMessage, '16')
                time.sleep(0.1)

    #write on CAN
    def write(self, message, ID):
        # We create a TPCANMsg message structure
        CANMsg = TPCANMsg()

        # We configurate the Message.  The ID,
        # Length of the Data, Message Type and the data

        CANMsg.ID = int(ID)
        CANMsg.LEN = len(message)
        CANMsg.MSGTYPE = PCAN_MESSAGE_STANDARD

        for i in range(CANMsg.LEN):
            CANMsg.DATA[i] = int(message[i])

        # The message is sent to the configured hardware

        self.m_objPCANBasic.Write(self.m_bus, CANMsg)

    #get CAN status
    def GetStatus(self):
        status = self.m_objPCANBasic.GetStatus(self.m_bus)
        return status

    #reset CAN
    def reset(self):
        self.m_objPCANBasic.Reset(self.m_bus)

    #log thread
    def logFile(self):
        f1 = open('mikro1.txt','a+')
        f2 = open('mikro2.txt','a+')
        f3 = open('mikro3.txt','a+')
        f4 = open('mikro4.txt','a+')
        f5 = open('mikro5.txt','a+')

        lengthf1 = len(f1.readlines())
        lengthf2 = len(f2.readlines())
        lengthf3 = len(f3.readlines())
        lengthf4 = len(f4.readlines())
        lengthf5 = len(f5.readlines())

        if lengthf1 < self.lenghtLogFile:               #check File
            if lengthf1 > self.lenghtLogFile-2:         #delet next file if this is full
                f2.close()
                f2 = open('mikro2.txt','w')
            f1.write(str(localtime().tm_year))
            f1.write('/')
            f1.write(str(localtime().tm_mon))
            f1.write('/')
            f1.write(str(localtime().tm_mday))
            f1.write('/')
            f1.write(str(localtime().tm_hour))
            f1.write('/')
            f1.write(str(localtime().tm_min))
            f1.write('/')
            f1.write(str(localtime().tm_sec))
            f1.write(': ')
            f1.writelines(list( "%s, " % item for item in self.microdata ))
            f1.write('\n')

        elif lengthf2 < self.lenghtLogFile:             #check File
            if lengthf2 > self.lenghtLogFile-2:         #delet next file if this is full
                f3.close()
                f3 = open('mikro3.txt','w')
            f2.write(str(localtime().tm_year))
            f2.write('/')
            f2.write(str(localtime().tm_mon))
            f2.write('/')
            f2.write(str(localtime().tm_mday))
            f2.write('/')
            f2.write(str(localtime().tm_hour))
            f2.write('/')
            f2.write(str(localtime().tm_min))
            f2.write('/')
            f2.write(str(localtime().tm_sec))
            f2.write(': ')
            f2.writelines(list( "%s, " % item for item in self.microdata ))
            f2.write('\n')

        elif lengthf3 < self.lenghtLogFile:             #check File
            if lengthf3 > self.lenghtLogFile-2:         #delet next file if this is full
               f4.close()
               f4 = open('mikro4.txt','w')
            f3.write(str(localtime().tm_year))
            f3.write('/')
            f3.write(str(localtime().tm_mon))
            f3.write('/')
            f3.write(str(localtime().tm_mday))
            f3.write('/')
            f3.write(str(localtime().tm_hour))
            f3.write('/')
            f3.write(str(localtime().tm_min))
            f3.write('/')
            f3.write(str(localtime().tm_sec))
            f3.write(': ')
            f3.writelines(list( "%s, " % item for item in self.microdata ))
            f3.write('\n')

        elif lengthf4 < self.lenghtLogFile:             #check File
            if lengthf4 > self.lenghtLogFile-2:         #delet next file if this is full
                f5.close()
                f5 = open('mikro5.txt','w')
            f4.write(str(localtime().tm_year))
            f4.write('/')
            f4.write(str(localtime().tm_mon))
            f4.write('/')
            f4.write(str(localtime().tm_mday))
            f4.write('/')
            f4.write(str(localtime().tm_hour))
            f4.write('/')
            f4.write(str(localtime().tm_min))
            f4.write('/')
            f4.write(str(localtime().tm_sec))
            f4.write(': ')
            f4.writelines(list( "%s, " % item for item in self.microdata ))
            f4.write('\n')

        elif lengthf5 < self.lenghtLogFile:             #check File
            if lengthf5 > self.lenghtLogFile-2:         #delet next file if this is full
               f1.close()
               f1 = open('mikro1.txt','w')
            f5.write(str(localtime().tm_year))
            f5.write('/')
            f5.write(str(localtime().tm_mon))
            f5.write('/')
            f5.write(str(localtime().tm_mday))
            f5.write('/')
            f5.write(str(localtime().tm_hour))
            f5.write('/')
            f5.write(str(localtime().tm_min))
            f5.write('/')
            f5.write(str(localtime().tm_sec))
            f5.write(': ')
            f5.writelines(list( "%s, " % item for item in self.microdata ))
            f5.write('\n')

        f1.close()
        f2.close()
        f3.close()
        f4.close()
        f5.close()
