__author__ = 'David'


class Input:
    #Constuctor
    def __init__(self,gui,can):
        self.m_gui= gui
        self.m_can = can

    #refresh all Display/ Labels
    def refreshDisplay(self):
        #init
        if self.m_can.GetStatus() <10:
            self.m_gui.setColorinit("green")
            self.m_gui.setValueActual(self.m_can.microdata[1]*100/220)
            self.m_gui.setValueReflexion(self.m_can.microdata[6]*100/220)
            if self.m_can.analogdata[5] > 1.1:
                self.m_gui.setValueLoad(int(self.m_can.analogdata[5]*self.m_can.analogdata[5]*0.1922+9.44*self.m_can.analogdata[5]+53.227))
            else:
                self.m_gui.setValueLoad(0)
            if self.m_can.analogdata[7] != 0:
                self.m_gui.setValueWatertemp(self.m_can.analogdata[7]*50/127-15)
            else:
                self.m_gui.setValueWatertemp(0)
        else:
            self.m_gui.setColorinit("red")
            self.m_gui.setColormains("orange")
            self.m_gui.setColorfilament("orange")
            self.m_gui.setColormw("orange")
            self.m_gui.setColorfailure("orange")
        #mains
        if self.m_can.microdata[0] & 1:
            self.m_gui.setColormains("green")
        else:
            self.m_gui.setColormains("orange")
        #filament
        if self.m_can.microdata[0] & 2:
            self.m_gui.setColorfilament("green")
        else:
            self.m_gui.setColorfilament("orange")
        #microwave
        if self.m_can.microdata[0] & 4:
            self.m_gui.setColormw("green")
        else:
            self.m_gui.setColormw("orange")
        #interlock
        if self.m_can.microMessage[0] & 8:
            self.m_gui.setColorInterlock("green")
        else:
            self.m_gui.setColorInterlock("orange")
        #pump
        if self.m_can.pumpdata[1] == 03:
            self.m_gui.setColorpump("green")
        else:
            self.m_gui.setColorpump("orange")
        #pulsmode
        if self.m_can.microMessage[3] != 0 or self.m_can.microMessage[4] != 0:
            self.m_gui.setColorPulsMode("green")
        else:
            self.m_gui.setColorPulsMode("orange")
        #failure
        if (self.m_can.microdata[0] & 16):
            self.m_gui.setColorfailure("red")
            if self.theFirstTime == 0:
                self.theFirstTime = 1
                self.mikroError()
        else:
            self.m_gui.setColorfailure("green")
            self.theFirstTime = 0
        #overload
        if self.m_can.microdata[0] & 32:
            self.m_gui.setColorOverload("red")
        else:
            self.m_gui.setColorOverload("green")
        #cooling
        if self.m_can.microdata[0] & 64:
            self.m_gui.setColorCooling("red")
        else:
            self.m_gui.setColorCooling("green")
        #watertemperatur
        if self.m_gui.watertemp.get() > 27:
            self.m_gui.setColorWaterlimit("red")
            self.m_can.microMessage[0] = 0
            self.m_can.microMessage[3] = 0
            self.m_can.microMessage[4] = 0
            self.m_gui.waterColour.set("red")
        elif self.m_gui.watertemp.get() > 22:
            self.m_gui.waterColour.set("orange")
        else:
            self.m_gui.waterColour.set("green")
        #reflexion limit
        if self.m_gui.reflexion.get() > 10:
            self.m_gui.setColorReflexion("red")
        else:
            self.m_gui.setColorReflexion("green")

    #error in Infobox
    def mikroError(self):
        if self.m_can.microdata[4] == 128:
            self.m_gui.writeInfo("ERROR 80: Kommunikation")
        elif self.m_can.microdata[4] == 129:
            self.m_gui.writeInfo("ERROR 81: Interlock")
        elif self.m_can.microdata[4] == 130:
            self.m_gui.writeInfo("ERROR: Not-Aus")
        elif self.m_can.microdata[4] == 131:
            self.m_gui.writeInfo("ERROR 83: Kuehlung")
        elif self.m_can.microdata[4] == 134:
            self.m_gui.writeInfo("ERROR 86: Anodenstrom zu hoch")
        elif self.m_can.microdata[4] == 135:
            self.m_gui.writeInfo("ERROR 87: Anodenspannung zu hoch")
        elif self.m_can.microdata[4] == 137:
            self.m_gui.writeInfo("ERROR 89: Zu wenig Leistung")
        elif self.m_can.microdata[4] == 138:
            self.m_gui.writeInfo("ERROR 8A: Kurzschluss im Magnetron")
        elif self.m_can.microdata[4] == 255:
            self.m_gui.writeInfo("ERROR FF: Nicht mehr Betriebsbereit -> Service")
