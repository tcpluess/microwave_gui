from Tkinter import *
import ttk                     ## TK extensions library
from Output import *
from Input import *
from threading import Thread
from thread import allocate_lock
import time
from CAN import *
#

class GUI(object):
    ## Constructor
    ##

    def __init__(self):
        # Master's configuration
        master = Tk()
        self.m_Master = master
        self.m_Master.wm_title('Mikrowelle als Prozessenergie')
        self.m_Master.resizable(False,False)

        # Frame's configuration
        self.m_Frame =Frame(self.m_Master)
        self.m_Frame.grid(row=0, column=0, padx=5, pady=2, sticky="nwes")
        # Configuration
        self.val = 0
        self.val1 = 0
        self.val2 = 0
        self.watertemp = IntVar()
        self.aktvalue = IntVar()
        self.reflexion = IntVar()
        self.load = IntVar()
        self.InitializeBasicComponents()
        self.CenterTheWindow()
        self.InitializeWidgets()
        self.t = Thread(target=self.prog, args=())

        master.mainloop()
        self.m_out.release()
        #Log Mikrowellen Life Time
        self.f = open('MikroLifeTime.txt', 'w')
        self.f.write(str(self.microLifeTime.get()))
        self.f.close()

    ## Destructor
    ##
    def destroy (self):
        self.m_out.release()

    # Initialize Objects, Variables
    def InitializeBasicComponents(self):
        self.m_can = CAN(self)
        self.m_out = Output(self, self.m_can)
        self.m_in = Input(self, self.m_can)
        self.m_Width =760
        self.m_Height = 700
        self.f = open('MikroLifeTime.txt', 'r')
        self.microLifeTime = IntVar()
        self.microLifeTime.set(self.f.read())
        self.f.close()
        self.microtime = IntVar()
        self.siginit = StringVar()
        self.siginit.set("red")

        #Status
        self.sigmains = StringVar()
        self.sigmains.set("orange")
        self.sigfilament = StringVar()
        self.sigfilament.set("orange")
        self.sigpump = StringVar()
        self.sigpump.set("orange")
        self.siginterlock = StringVar()
        self.siginterlock.set("orange")
        self.sigmw = StringVar()
        self.sigmw.set("orange")
        self.sigpulsmode = StringVar()
        self.sigpulsmode.set("orange")

        #Warnings
        self.sigFailure = StringVar()
        self.sigFailure.set("green")
        self.sigOverload = StringVar()
        self.sigOverload.set("green")
        self.sigCooling = StringVar()
        self.sigCooling.set("green")
        self.sigWaterlimit = StringVar()
        self.sigWaterlimit.set("green")
        self.sigReflexion = StringVar()
        self.sigReflexion.set("green")


        self.pulson1 = IntVar()
        self.pulson2 = StringVar()
        self.pulsoff1 = IntVar()
        self.pulsoff2 = StringVar()
        self.power1 = IntVar()
        self.power2 = StringVar()

        self.waterColour = StringVar()
        self.waterColour.set("green")
        self.s = ttk.Style()


    #Initialize Widgets
    def InitializeWidgets(self):
        # Connection

        self.Connection = LabelFrame(self.m_Frame, height=70, width = 745, text=" Connection ")
        self.Connection.grid_propagate(0)
        self.Connection.grid(row=0, column = 0, padx=2, pady=2)
        self.InitializeConnectionWidgets()
        self.InitializeConnectionLabels()

        self.Signals = LabelFrame(self.m_Frame, height=200, width = 745, text=" Signals ")
        self.Signals.grid_propagate(0)
        self.Signals.grid(row=1, column = 0, padx=2, pady=2)
        self.InitializeSignalsWidgets()
        self.InitializeStatusLabels()

        ## Configuration Parameters groupbox
        self.Parameters = LabelFrame(self.m_Frame, height=280, width = 745, text=" Configuration Parameters ")
        self.Parameters.grid_propagate(0)
        self.Parameters.grid(row=2, column = 0, padx=2, pady=2)
        self.InitializeConfigurationWidgets()
        self.InitializeConfigurationLabels()

        ## Information groupbox
        self.gbInfo = LabelFrame(self.m_Frame, height=130, width = 745, text=" Information ")
        self.gbInfo.grid_propagate(0)
        self.gbInfo.grid(row=5, column = 0, padx=2, pady=2)
        self.InitializeInformationWidgets()

    #Initialize Connection Widgets
    def InitializeConnectionWidgets(self):

        self.btninit = Button(self.Connection, text="CAN Initialize", command=self.m_out.btninit_Click, padx=20)
        self.btninit.grid(row=0, column=1, sticky=W, padx=10, pady=10)
        self.btnrelease = Button(self.Connection, text="CAN Release", command=self.m_out.btnrelease_Click, padx=20)
        self.btnrelease.grid(row=0, column=2, sticky=W, padx=10, pady=10)
        self.label0 = Label(self.Connection, relief='ridge', padx=10)
        self.label0.grid(row=0, column=3, sticky=E, padx=30)

    #Configure Connection Label
    def InitializeConnectionLabels(self):
        self.label0.config(bg=self.siginit.get())

    #Initialize Status/Signal Widgets
    def InitializeSignalsWidgets(self):

        self.btnreset = Button(self.Signals, text="Reset", padx=27, command=self.m_out.btnreset_Click).grid(row=0, column=0, sticky=W, padx=20)
        self.btnfilament =Button(self.Signals, text="Filament  ", padx=15, command=self.m_out.btnfilament_Click).grid(row=2, column=0, sticky=W, padx=20)
        self.btnpump = Button(self.Signals, text="Pump  ", padx=22, command=self.m_out.btnpump_Click).grid(row=3, column=0, sticky=W, padx=20)
        self.btninterlock = Button(self.Signals, text="Interlock  ", padx=15, command=self.m_out.btninterlock_Click).grid(row=4, column=0, sticky=W, padx=20)
        self.btnpulsmode = Button(self.Signals, text="Pulsmode   ", padx=10, command=self.m_out.btnpuls_Click).grid(row=5, column=0, sticky=W, padx=20)
        self.btnmicro = Button(self.Signals, text="Micromave  ",padx=8, command=self.m_out.btnmicro_Click).grid(row=6, column=0, sticky=W, padx=20)
        #Status
        Label(self.Signals, text="Status", font=40).grid(row=0, column=1, sticky=W, columnspan=2)
        self.label1 = Label(self.Signals, relief='ridge', padx=10)
        self.label1.grid(row=1, column=1, sticky=W)
        Label(self.Signals, text="Mains").grid(row=1, column=2, sticky=W)
        self.label2 = Label(self.Signals, relief='ridge', padx=10)
        self.label2.grid(row=2, column=1, sticky=W)
        Label(self.Signals, text="Filament ON").grid(row=2, column=2, sticky=W)
        self.label3 = Label(self.Signals, relief='ridge', padx=10)
        self.label3.grid(row=3, column=1, sticky=W)
        Label(self.Signals, text="Pump ON").grid(row=3, column=2, sticky=W)
        self.label4 = Label(self.Signals, relief='ridge', padx=10)
        self.label4.grid(row=4, column=1, sticky=W)
        Label(self.Signals, text="Interlock").grid(row=4, column=2, sticky=W)
        self.label5 = Label(self.Signals, relief='ridge', padx=10)
        self.label5.grid(row=5, column=1, sticky=W)
        Label(self.Signals, text="Puls Mode ON        ").grid(row=5, column=2, sticky=W)
        self.label6 = Label(self.Signals, relief='ridge', padx=10)
        self.label6.grid(row=6, column=1, sticky=W)
        Label(self.Signals, text="MW ON").grid(row=6, column=2, sticky=W)
        #Warning
        Label(self.Signals, text="Warnings", font=20).grid(row=0, column=3, sticky=W, columnspan=2)
        self.label7  = Label(self.Signals, relief='ridge', padx=10)
        self.label7.grid(row=1, column=3, sticky=W)
        Label(self.Signals, text="Failure").grid(row=1, column=4, sticky=W)
        self.label8 = Label(self.Signals, relief='ridge', padx=10)
        self.label8.grid(row=2, column=3, sticky=W)
        Label(self.Signals, text="Overload").grid(row=2, column=4, sticky=W)
        self.label9 = Label(self.Signals, relief='ridge', padx=10)
        self.label9.grid(row=3, column=3, sticky=W)
        Label(self.Signals, text="Cooling").grid(row=3, column=4, sticky=W)
        self.label10 = Label(self.Signals, relief='ridge', padx=10)
        self.label10.grid(row=4, column=3, sticky=W)
        Label(self.Signals, text="Watertemperatur Limit").grid(row=4, column=4, sticky=W)
        self.label11 = Label(self.Signals, relief='ridge', padx=10)
        self.label11.grid(row=5, column=3, sticky=W)
        Label(self.Signals, text="Reflexion Limit").grid(row=5, column=4, sticky=W)

        #Run/Lifetime
        Label(self.Signals, textvariable= self.microtime, relief='ridge', padx=10).grid(row=1, column=6, sticky=W)
        Label(self.Signals, textvariable= self.microLifeTime, relief='ridge', padx=10).grid(row=2, column=6, sticky=W)

        Label(self.Signals, text="min       Microwave Runtime").grid(row=1, column=7, sticky=W)
        Label(self.Signals, text="min       Microwave Lifetime").grid(row=2, column=7, sticky=W)

    #Configure Status Label
    def InitializeStatusLabels(self):
        self.label1.config(bg=self.sigmains.get())
        self.label2.config(bg=self.sigfilament.get())
        self.label3.config(bg=self.sigpump.get())
        self.label4.config(bg=self.siginterlock.get())
        self.label5.config(bg=self.sigpulsmode.get())
        self.label6.config(bg=self.sigmw.get())
        #Warning
        self.label7.config(bg=self.sigFailure.get())
        self.label8.config(bg=self.sigOverload.get())
        self.label9.config(bg=self.sigCooling.get())
        self.label10.config(bg=self.sigWaterlimit.get())
        self.label11.config(bg=self.sigReflexion.get())

    #Initialize Configuration/Display Widgets
    def InitializeConfigurationWidgets(self):

        Scale(self.Parameters, from_=24, to=0, orient=VERTICAL,length = 150, tickinterval=10, variable = self.pulson1).grid(row=1, column=0, sticky=W,padx = 20,pady = 20)
        Spinbox(self.Parameters, width=10, from_=0, to=24, textvariable =self.pulson2).grid(row=2, column=0,padx=20, pady=2)
        Label(self.Parameters, text="Puls ON").grid(row=3, column=0, sticky=W,padx = 20)

        Scale(self.Parameters, from_=250, to=0, orient=VERTICAL,length = 150,tickinterval=100, variable = self.pulsoff1).grid(row=1, column=1, sticky=W,padx = 20)
        Spinbox(self.Parameters, width=10, from_=0, to=250, textvariable = self.pulsoff2).grid(row=2, column=1,padx=20, pady=2)
        Label(self.Parameters, text="Puls OFF").grid(row=3, column=1, sticky=W,padx = 20)

        Scale(self.Parameters, from_=100, to=0, orient=VERTICAL,length = 150,tickinterval=50, variable = self.power1).grid(row=1, column=2, sticky=W,padx = 20)
        Spinbox(self.Parameters, width=10, from_=0, to=100, textvariable = self.power2).grid(row=2, column=2,padx=20, pady=2)
        Label(self.Parameters, text="Set Value").grid(row=3, column=2, sticky=W,padx = 20)

        d = ttk.Style()
        d.theme_use('clam')
        d.configure("green.Vertical.TProgressbar", background='green')
        ttk.Progressbar(self.Parameters, style= "green.Vertical.TProgressbar", orient=VERTICAL, length=150, mode='determinate',variable=self.aktvalue).grid(row=1, column=3, sticky=W,padx=30)
        Label(self.Parameters, textvariable=self.aktvalue, relief='ridge',padx = 30).grid(row=2, column=3, sticky=W,padx = 10)
        Label(self.Parameters, text="Actual Value [%]").grid(row=3, column=3, sticky=W)
        ttk.Progressbar(self.Parameters,style= "green.Vertical.TProgressbar", orient=VERTICAL, length=150, mode='determinate',variable=self.reflexion).grid(row=1, column=4, sticky=W,padx=30)
        Label(self.Parameters, textvariable=self.reflexion, relief='ridge',padx = 30).grid(row=2, column=4, sticky=W,padx = 10)
        Label(self.Parameters, text="Reflexion [%]").grid(row=3, column=4, sticky=W,padx = 10)
        Label(self.Parameters, text="max. 2000W").grid(row=0, column=5, sticky=W)
        ttk.Progressbar(self.Parameters, style= "green.Vertical.TProgressbar", orient=VERTICAL, length=150, maximum = 2000, mode='determinate',variable=self.load).grid(row=1, column=5, sticky=W,padx=30)
        Label(self.Parameters, textvariable=self.load, relief='ridge',padx = 30).grid(row=2, column=5, sticky=W,padx = 10)
        Label(self.Parameters, text="Load [W]").grid(row=3, column=5, sticky=W,padx = 25)
        Label(self.Parameters, text="max. 35C").grid(row=0, column=6, sticky=W)

        self.s.theme_use('clam')
        ttk.Progressbar(self.Parameters, style="Vertical.TProgressbar",orient=VERTICAL, length=150, maximum = 50, mode='determinate', variable=self.watertemp).grid(row=1, column=6, sticky=W,padx=30)
        Label(self.Parameters, textvariable=self.watertemp, relief='ridge',padx = 30).grid(row=2, column=6, sticky=W,padx = 10)
        Label(self.Parameters, text="Watertemp [C]").grid(row=3, column=6, sticky=W,padx = 10)

    #Configure Display Label
    def InitializeConfigurationLabels(self):
        self.s.configure("Vertical.TProgressbar", foreground='green', background=self.waterColour.get())

    #Initialize Information Widgets
    def InitializeInformationWidgets(self):
        # Controls
        #
        self.yInfoScroll = Scrollbar(self.gbInfo, orient=VERTICAL)
        self.yInfoScroll.grid(row=0, column=1, sticky=N+S)

        self.lbxInfo = Listbox(self.gbInfo, width=100, height=6, activestyle="none", yscrollcommand=self.yInfoScroll.set)
        self.lbxInfo.grid(row=0, column = 0, padx=5, sticky="nwes")
        self.lbxInfo.bind("<Double-1>", self.btnInfoClear_Click)

        self.yInfoScroll['command'] = self.lbxInfo.yview
        self.writeInfo("Click CAN Initialize")

        self.btnInfoClear = Button(self.gbInfo, width = 8, text = "Clear", command=self.btnInfoClear_Click)
        self.btnInfoClear.grid(row=0, column=4, sticky=NE,padx = 20)
        self.btnMsg = Button(self.gbInfo, width = 8, text = "Messages", command=self.btnMsg_Click)
        self.btnMsg.grid(row=0, column=4, pady=25, sticky=W,padx = 20)

    #Write Info in Information Widget
    def writeInfo(self, Infomsg):
         self.lbxInfo.insert(0,Infomsg)

    #Clear Information Widget Button
    def btnInfoClear_Click(self, event=None):
        # The information contained in the Information List-Box
        # is cleared
        #
        self.lbxInfo.delete(0,END)

    #Show Messages from CAN-Bus
    def btnMsg_Click(self):
        self.writeInfo(self.m_can.microMessage)
        self.writeInfo("Mikrowelle Output:")
        self.writeInfo(self.m_can.pumpdata)
        self.writeInfo("Pumpe Input:")
        self.writeInfo(self.m_can.microdata)
        self.writeInfo("Mikrowelle Input:")
        self.writeInfo(self.m_can.analogdata)
        self.writeInfo("Analog Input:")

    #Center Window
    def CenterTheWindow(self):
            """

            :rtype :
            """
            Desktop = self.m_Master.winfo_toplevel()
            desktopWidth = Desktop.winfo_screenwidth()
            desktopHeight = Desktop.winfo_screenheight()

            self.m_Master.geometry("{0}x{1}+{2}+{3}".format(self.m_Width,
                                                            self.m_Height,
                                                            (desktopWidth-self.m_Width)/2,
                                                            (desktopHeight-self.m_Height)/2))
    #Set Color from Label
    def setColorinit(self, k):
        self.siginit.set(k)

    def setColormains(self, k):
        self.sigmains.set(k)

    def setColorfilament(self, k):
        self.sigfilament.set(k)

    def setColorpump(self, k):
        self.sigpump.set(k)

    def setColorInterlock(self, k):
        self.siginterlock.set(k)

    def setColormw(self, k):
        self.sigmw.set(k)

    def setColorPulsMode(self, k):
        self.sigpulsmode.set(k)

    def setColorfailure(self, k):
        self.sigFailure.set(k)

    def setColorOverload(self, k):
        self.sigOverload.set(k)

    def setColorCooling(self, k):
        self.sigCooling.set(k)

    def setColorWaterlimit(self, k):
        self.sigWaterlimit.set(k)

    def setColorReflexion(self, k):
        self.sigReflexion.set(k)

    #Set Value from Display
    def setValueActual(self, k):
        self.aktvalue.set(k)

    def setValueReflexion(self, k):
        self.reflexion.set(k)

    def setValueLoad(self, k):
        self.load.set(k)

    def setValueWatertemp(self, k):
        self.watertemp.set(k)

    #Configure Display Thread
    def prog(self):
        counter = 0
        while(self.m_can.GetStatus() < 10):
            self.setValuePulsOn()
            self.setValuePulsOff()
            self.setValuePower()
            time.sleep(0.05)
            if self.m_can.microdata[0] == 07:
                counter = counter +1
                if counter == 60:
                    counter = 0
                    self.microtime.set(self.microtime.get() + 1)
                    self.microLifeTime.set(self.microLifeTime.get()+ 1)
            self.m_in.refreshDisplay()
            self.InitializeConnectionLabels()
            self.InitializeStatusLabels()
            self.InitializeConfigurationLabels()

    #For Spinbox and Scale
    def setValuePulsOn(self):
        if self.m_can.microdata[0] != 07:
            if self.val != self.pulson1.get():
                self.val = self.pulson1.get()
                self.pulson2.set(self.val)
            elif self.val != self.pulson2.get():
                self.val = int(self.pulson2.get())
                self.pulson1.set(self.val)
        else:
            self.pulson1.set(self.val)
            self.pulson2.set(self.val)

    def setValuePulsOff(self):
        if self.m_can.microdata[0] != 07:
            if self.val1 != self.pulsoff1.get():
                self.val1 = self.pulsoff1.get()
                self.pulsoff2.set(self.val1)
            elif self.val1 != self.pulsoff2.get():
                self.val1 = int(self.pulsoff2.get())
                self.pulsoff1.set(self.val1)
        else:
            self.pulsoff1.set(self.val1)
            self.pulsoff2.set(self.val1)

    def setValuePower(self):
        if self.m_can.microdata[0] != 07:
            if self.val2 != self.power1.get():
                self.val2 = self.power1.get()
                self.power2.set(self.val2)
            elif self.val2 != self.power2.get():
                self.val2 = int(self.power2.get())
                self.power1.set(self.val2)
        else:
            self.power1.set(self.val2)
            self.power2.set(self.val2)
